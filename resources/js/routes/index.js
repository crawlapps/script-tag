import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/script/List').default,
        name:'scripts',
        meta: {
            title: 'Scripts',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/addscripts',
        component: require('../components/pages/script/AddScript').default,
        name:'addscripts',
        meta: {
            title: 'Add Scripts',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/plan-pricing',
        component: require('../components/pages/Plan').default,
        name:'plan-pricing',
        meta: {
            title: 'Plan & Pricing',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/instructions',
        component: require('../components/pages/Instructions').default,
        name:'instructions',
        meta: {
            title: 'Instructions',
            description: '',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
];

const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

// router.beforeEach((to, from, next) => {
//     console.log(to);
//     return next();
// });
export default router;
