<?php

if (!function_exists('shShop')) {
    function shShop()
    {
        return \ShopifyApp::shop();
    }
}

if (! function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @return string
     */
    function str_slug($string)
    {
        return preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    }
}
