<?php

namespace App\Model;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class ScriptTag extends Model
{
    //

    public function hasManyResource()
    {
        return $this->hasMany(ResourceData::class, 'script_tag_id', 'id' );
    }
}
