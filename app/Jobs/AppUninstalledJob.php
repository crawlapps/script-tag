<?php namespace App\Jobs;

use App\Model\ResourceData;
use App\Model\ScriptTag;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use OhMyBrew\ShopifyApp\Models\Shop;

class AppUninstalledJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \Log::info('--------------- app uninstall ---------------------');
            $shop = Shop::where('shopify_domain', $this->shopDomain)->first();

            $scripts = ScriptTag::where('shop_id', $shop->id)->get();
            if( $scripts->count() > 0 ){
                $theme_id = $shop->theme_id;
                foreach ($scripts as $key=>$val){
                    \Log::info(json_encode($val));

//                    $this->removeSnippet($shop, $theme_id, $val->snippet_key);
//                    $this->removeLiquidCode($shop, $theme_id, $val->slug);
                    $val->delete();
                }
            }

            $resource = ResourceData::where('shop_id', $shop->id)->get();
            if ($resource->count() > 0) {
                foreach ($resource as $val) {
                    $val->delete();
                }
            }
        }catch(\Exception $e){
            \Log::info('--------------- ERROR:: handle ---------------------');
            \Log::info(json_encode($e));
        }
    }

    public function removeSnippet($shop, $theme_id, $snippet_key){
        try {
            \Log::info('--------------- removeSnippet ---------------------');
            // delete snippet
            $parameter['asset']['key'] = $snippet_key;
            $result = $shop->api()->rest('DELETE', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            \Log::info(json_encode($result));
        }catch(\Exception $e){
            \Log::info('--------------- ERROR:: removeSnippet ---------------------');
            \Log::info(json_encode($e));
        }
    }

    public function removeLiquidCode( $shop, $theme_id, $snippet_name){
        try {
            \Log::info('--------------- removeLiquidCode ---------------------');
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
                ["asset[key]" => 'layout/theme.liquid']);
            if (@$asset->body->asset) {
                $asset = $asset->body->asset->value;

                // remove if exist
                if (strpos($asset, "<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End: insert_tag_code--></head>")) {
                    $asset = str_replace(
                        "<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End: insert_tag_code --></head>",
                        '</head>', $asset);
                }

                if (strpos($asset, "<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End: insert_tag_code --></body>")) {
                    $asset = str_replace("<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End: insert_tag_code --></body>", '</body>', $asset);
                }

                $srch_string = substr(stristr($asset, '<body'), 0, strpos(stristr($asset, '<body'), '>') + 1);

                if (strpos($asset, "$srch_string<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End: insert_tag_code -->")) {
                    $asset = str_replace("$srch_string<!-- insert_tag_code -->{% include '$snippet_name' %}<!-- End:insert_tag_code -->", $srch_string, $asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
                \Log::info(json_encode($result));
            }
        }catch(\Exception $e){
            \Log::info('--------------- ERROR:: removeLiquidCode ---------------------');
            \Log::info(json_encode($e));
        }
    }

    public function getTheme($shop){
        try {
            \Log::info('--------------- getTheme ---------------------');
            $main_theme = $shop->api()->rest('GET', 'admin/themes.json', ['role' => 'main']);
            \Log::info(json_encode($main_theme));

            return  $main_theme->body->themes[0]->id;

        }catch(\Exception $e){
            \Log::info('--------------- ERROR:: getTheme ---------------------');
            \Log::info(json_encode($e));
        }
    }
}
