<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddSnippetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('--------------------- add snippet --------------------');
        $type = 'add';
        $shop = \ShopifyApp::shop();
        $parameter['role'] = 'main';
        $result = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/themes.json',$parameter);

        \Log::info(json_encode($result));
        $theme_id = $result->body->themes[0]->id;
        \Log::info('Theme id :: ' . $theme_id);
        if($type == 'add') {
            $value = <<<EOF
        <script id="script_tag_data" type="application/json">
            {
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }}
                "page": {{ page | json }},
                "blog": {{ blog | json }},
                "article": {{ article | json }},
            }
        </script>
EOF;
        }
        $parameter['asset']['key'] = 'snippets/script_tag.liquid';
        $parameter['asset']['value'] = $value;
        $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id . '/assets.json',$parameter);
        \Log::info('--------------- snippet --------------------');
        \Log::info(json_encode($asset));
    }
}
