<?php

namespace App\Http\Controllers\Script;

use App\Http\Controllers\Controller;
use App\Http\Requests\ScriptRequest;
use App\Model\ResourceData;
use App\Model\ScriptTag;
use DOMDocument;
use Illuminate\Http\Request;
use Response;

class ScriptController extends Controller
{
    private $shop = '';

    public function index(Request $request)
    {
        if ($request->has('api')) {
            return $this->json($request);
        }
    }

    public function json($request)
    {
        try {
            $this->shop = shShop();

            if ($request->title  != '' && @$request->filter) {
                $entities = ScriptTag::select('id', 'title', 'script', 'condition', 'status',
                    'created_at')->where('shop_id', $this->shop->id)->where('title', 'LIKE', '%'.$request->title.'%')->where($request->filter, $request->value)->get();
            } elseif (@$request->filter) {
                $entities = ScriptTag::select('id', 'title', 'script', 'condition', 'status',
                    'created_at')->where('shop_id', $this->shop->id)->where($request->filter, $request->value)->get();
            } elseif ( $request->title  != '' ){
                $entities = ScriptTag::select('id', 'title', 'script', 'condition', 'status',
                    'created_at')->where('shop_id', $this->shop->id)->where('title', 'LIKE', '%'.$request->title.'%')->get();
            }else {
                $entities = ScriptTag::select('id', 'title', 'script', 'condition', 'status',
                    'created_at')->where('shop_id', $this->shop->id)->get();
            }

            if ($entities) {
                $entity = $entities->map(function ($field) {
                    $position = ($field->condition == 0) ? 'Header' : 'Footer';
                    $position = ($field->condition == 1) ? 'Body' : $position;
                    return [
                        'id' => $field->id,
                        'title' => $field->title,
                        'position' => $position,
                        'status' => ($field->status === 0) ? 'Deactive' : 'Active',
                        'published_at' => date("Y-m-d H:i:s", strtotime($field->created_at)),
                    ];
                })->toArray();
            } else {
                $entity = [];
            }
            return response::json(['data' => $entity], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    public function saveScript(ScriptRequest $request)
    {
        try {

            $this->shop = shShop();
            $data = $request->all();
            $data = $data['data'];

            if ($data['id']) {
                $script_tag = ScriptTag::find($data['id']);

                $resourceData = ResourceData::where('script_tag_id', $script_tag->id)->get();
                if ($resourceData->count() > 0) {
                    foreach ($resourceData as $val) {
                        $val->delete();
                    }
                }
            } else {
                $script_tag = new ScriptTag;
                $script_tag->shop_id = $this->shop->id;
                $script_tag->save();
            }

            $snippet_name = 'script-tag-' . strtolower(str_slug($data['title'])) . '-' . $script_tag->id;
            $snippetValue = ( $data['status'] == 1 ) ? $this->createSnippetValue($data) : '';

            $theme_id = $this->getTheme();
            $snippet = $this->createSnippet($snippetValue, $snippet_name, $theme_id);

            if( !$snippet['error'] ){
                $this->updateThemeLiquid($theme_id, $data['condition'], $snippet_name, $this->shop, 'add');

                foreach( $data['show_on'] as $key => $resource ){
                    if ($resource){
                        $show_on[] = $key;
                    }
                }
                    $script_tag->shop_id = $this->shop->id;
                    $script_tag->title = $data['title'];
                    $script_tag->script = $data['script'];
                    $script_tag->condition = $data['condition'];
                    $script_tag->show_on = implode(',', $show_on);
                    $script_tag->status = $data['status'];
                    $script_tag->snippet_key = $snippet['key'];
                    $script_tag->slug = $snippet_name;

                    $script_tag->save();
                    foreach( $data['show_on'] as $key => $resource ){
                        if ($key != 'all' && $key != 'front_page') {
                            if ($resource) {
                                foreach ($data['resourceList'][$key] as $val) {
                                    if( $val ) {
                                        $resource = new ResourceData;
                                        $resource->shop_id = $this->shop->id;
                                        $resource->script_tag_id = $script_tag->id;
                                        $resource->resource = $key;
                                        $resource->url = '/' . $key . '/' . basename($val);
                                        $resource->save();
                                    }
                                }
                            }
                        }
                    }
            }else{
                return response()->json(['data' => 'Script not saved!!'], 422);
            }
            return response()->json(['data' => 'Script code added successfully.'], 200);
        } catch (\Exception $e) {
            dump('------------ERROR :: saveScript--------------');
            dd($e);
            return response()->json(['data' => $e], 422);
        }
    }

    public function createSnippetValue($data)
    {
        try {
            $snippetValue = '';
            if ($data['show_on']['all']) {
                $snippetValue = $data['script'];
            } else {
                $handles = [];
                foreach ($data['show_on'] as $key => $resource) {
                    if ($key != 'all') {
                        if ($resource) {
                            if ($key != 'front_page') {
                                foreach ($data['resourceList'][$key] as $k=>$val) {
                                    $val =  strtok($val, '?');
                                    $handles[$key][] = '/' . $key . '/' . basename($val);
                                }
                            }else{
                                $script = $data['script'];
                                $snippetValue =  "{% if template  == 'index' %}$script{% endif %}";
                            }
//                            $snippetValue = $snippetValue.$this->$key($data['script'], $ids);
                        }
                    }
                }
                $snippetValue = $snippetValue.$this->getValue($data, $data['script'], $handles, $key);
            }
            return $snippetValue;
        } catch (\Exception $e) {
            dump('------------ERROR :: createSnippetValue--------------');
            dd($e);
        }
    }

    public function createSnippet($snippetValue, $snippet_name, $theme_id)
    {
        try {
            $shop = shShop();

            $value = <<<EOF
    $snippetValue
EOF;

            $parameter['asset']['key'] = 'snippets/'.$snippet_name.'.liquid';
            $parameter['asset']['value'] = $value;
            $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            if ($result->errors) {
                $snippet['error'] = true;
            } else {
                $snippet['error'] = false;
                $snippet['key'] = $result->body->asset->key;
            }
            return $snippet;
        } catch (\Exception $e) {
            dump('------------ERROR :: createSnippetValue--------------');
            dd($e);
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function updateThemeLiquid($theme_id, $code_place, $snippet_name, $shop, $action)
    {
        try {
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
                ["asset[key]" => 'layout/theme.liquid']);
            if (@$asset->body->asset) {
                $asset = $asset->body->asset->value;

                if ($code_place == 0) {
                    $search_string = "</head>";
                }elseif ( $code_place == 1 ) {
                    $search_string = substr(stristr($asset, '<body'), 0, strpos(stristr($asset, '<body'), '>') + 1);
                } elseif ($code_place == 2) {
                    $search_string = '</body>';
                }

                // remove if exist
                if (strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace(
                        "{% include '$snippet_name' %}",'', $asset);
                }

                if (strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace("{% include '$snippet_name' %}",'', $asset);
                }

                $srch_string = substr(stristr($asset, '<body'), 0, strpos(stristr($asset, '<body'), '>') + 1);
                if (strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace("{% include '$snippet_name' %}", '', $asset);
                }

                // add
                if( $action == 'add' ){
                    if ($code_place == 0 || $code_place == 2) {
                        // add before </head> or </body>
                        if (!strpos($asset, "{% include '$snippet_name' %} \n$search_string")) {
                            $asset = str_replace($search_string,
                                "{% include '$snippet_name' %} \n$search_string ", $asset);
                        }
                    } else {

                        // add after <body>
                        if (!strpos($asset, "$search_string\n{% include '$snippet_name' %}")) {
                            $asset = str_replace($search_string, "$search_string\n{% include '$snippet_name' %}", $asset);
                        }
                    }
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            }
        } catch (\Exception $e) {
            dump('------------ERROR :: updateThemeLiquid--------------');
            dd($e);
        }

    }

    public function getTheme()
    {
        try {
            $shop = shShop();
            $main_theme = $shop->api()->rest('GET', 'admin/themes.json', ['role' => 'main']);

            return  $main_theme->body->themes[0]->id;
        } catch (\Exception $e) {
            dump('ERROR :: ------- getTheme ---------');
            dd($e);
        }
    }

    public function getValue($data, $script, $handles, $resource){
            foreach ( $handles as $key=>$val ){
                $c = 1;
                $str = "";
                foreach( $val as $k=>$v ) {
                    $str = ($c == 1) ? $str."current_url != '$v'" : $str.' and '."current_url != '$v'";
                    $c++;
                }
                $new[$key] = $str;
            }

            $products = ( @$new['products'] ) ? $new['products'] : '';
            $blogs = ( @$new['blogs'] ) ? $new['blogs'] : '';
            $collections = ( @$new['collections'] ) ? $new['collections'] : '';
            $pages = ( @$new['pages'] ) ? $new['pages'] : '';
            $articles = ( @$new['articles'] ) ? $new['articles'] : '';

           $v = "{% assign current_url = '' %}
                      {% case template %}
                      {% when 'page' %}
                      {% assign current_url = page.url %}";

           $v =  ( $pages != '' ) ? $v . " {% if $pages %} $script {% endif %}" : $v;
           $v = ( $data['show_on']['pages']  && count($data['resourceList']['pages']) == 0 ) ? $v . $script : $v;

           $v = $v . "{% when 'blog' %}
                      {% assign current_url = blog.url %}";
           $v =  ( $blogs != '' ) ? $v . " {% if $blogs %} $script {% endif %}" : $v;
           $v = ( $data['show_on']['blogs']  && count($data['resourceList']['blogs']) == 0 ) ? $v . $script : $v;

            $v = $v . "{% when 'article' %}
                          {% assign current_url = blog.url %}";
            $v =  ( $articles != '' ) ? $v . " {% if $articles %} $script {% endif %}" : $v;
            $v = ( $data['show_on']['articles']  && count($data['resourceList']['articles']) == 0 ) ? $v . $script : $v;

            $v = $v . "{% when 'collection' %}
                              {% assign current_url = collection.url %}";
            $v =  ( $collections != '' ) ? $v . " {% if $collections %} $script {% endif %}" : $v;
            $v = ( $data['show_on']['collections']  && count($data['resourceList']['collections']) == 0 ) ? $v . $script : $v;

            $v = $v . "{% when 'product' %}
                                  {% assign current_url = product.url %}";
            $v =  ( $products != '' ) ? $v . " {% if $products %} $script {% endif %}" : $v;
            $v = ( $data['show_on']['products']  && count($data['resourceList']['products']) == 0 ) ? $v . $script : $v;

            $v = $v . '{% endcase %}';
            return $v;
    }
    public function deleteScript(Request $request)
    {
        try {
            $shop = shShop();

            $script = ScriptTag::find($request->id);

            $theme_id = $this->getTheme();
            // delete snippet
            $parameter['asset']['key'] = $script->snippet_key;
            $result = $shop->api()->rest('DELETE', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            // delete snippet code from theme.liquid

            $this->updateThemeLiquid($theme_id, $script->condition, $script->slug, $shop, 'remove');

            // remove data from database
            ScriptTag::find($request->id)->delete();
            ResourceData::where('script_tag_id', $request->id)->delete();
            $msg = 'Deleted!';
            return response()->json(['data' => $msg], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function disableScript(Request $request)
    {
        try {

            $shop = shShop();
            $script = ScriptTag::find($request->id);
            $status = $script->status;
            if( $status == 1 ){
                $snippetValue = '';
                $script->status = 0;
                $msg = 'Script deactive!!';
            }else {
                $msg = 'Script active!!';
                $script->status = 1;
                $data = $this->getScript($request)->getData();
                $data = (array) $data->data;

                foreach ($data as $key => $val) {
                    if ($key == 'show_on' || $key == 'resourceList') {
                        $data[$key] = (array) $val;
                    }
                    if ($key == 'resourceList') {
                        foreach ($val as $rk => $rv) {
                            $data[$key][$rk] = (array) $rv;
                        }
                    }
                }
                $snippetValue = $this->createSnippetValue($data);
            }
            $theme_id = $this->getTheme();
            $snippet = $this->createSnippet($snippetValue, $script->slug, $theme_id);

            $script->save();
            return response()->json(['data' => $msg], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function getScript(Request $request)
    {
        try {
            $shop = shShop();
            $entity = ScriptTag::with('hasManyResource')->where('shop_id', $shop->id)->where('id',
                $request->id)->first();
            if ($entity) {
                $show_on = explode(',', $entity->show_on);
                $show = [
                    'all' => in_array('all', $show_on),
                    'front_page' => in_array('front_page', $show_on),
                    'products' => in_array('products', $show_on),
                    'collections' => in_array('collections', $show_on),
                    'pages' => in_array('pages', $show_on),
                    'blogs' => in_array('blogs', $show_on),
                    'articles' => in_array('articles', $show_on)
                ];

                if( $entity->hasManyResource ){
                    $resource = [
                        'products' => [],
                        'collections' => [],
                        'pages' => [],
                        'blogs' => [],
                        'articles' => [],
                    ];
                    foreach( $entity->hasManyResource as $key=>$val ){

//                        $d = [
//                            'resource_id'=> $val->resource_id,
//                            'title' => $val->title,
//                            'image' => $val->image,
//                            'handle' => $val->handle,
//                        ];
                        array_push($resource[$val->resource], $val->url);
                    }
                }
                $data = [
                        'id' => $entity->id,
                        'title' => $entity->title,
                        'script' => $entity->script,
                        'condition' => $entity->condition,
                        'show_on' => $show,
                        'status' => $entity->status,
                        'resourceList' => $resource,
                    ];
                }
//            dd($data);
            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function getResource(Request $request)
    {
        try {
            $shop = shShop();
            $resourceType = $request->rt;
            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/'.$resourceType.'.json';
            if( $request->s != '' ){
                $endPoint = '/admin/api/2019-04/'.$resourceType.'.json';
            }

            $parameter['title'] = $request->s;
            $parameter['fields'] = "id,title,handle";
            $result = $shop->api()->rest('GET', $endPoint);

            if ($result->body->$resourceType) {
                $data = $result->body->$resourceType;
                foreach ($data as $key => $val) {
                    $resource[$key]['resource_id'] = $val->id;
                    $resource[$key]['title'] = $val->title;
                    $resource[$key]['handle'] = $val->handle;
                    $resource[$key]['image'] = (@$val->image) ? $val->image->src : asset('static_upload/no-image-box.png');
                    $resource[$key]['is_checked'] = false;
                }
            } else {
                $resource = '';
            }
            return response()->json(['data' => $resource], 200);
        } catch (\Exception $e) {
            dd($e);
        }
    }

}
