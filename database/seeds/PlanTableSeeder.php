<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => 1,
                'name' => "Basic",
                'price' => 7.99,
                'capped_amount' => 0.00,
                'terms' => "Basic Plan",
                'trial_days' => 7,
                'test' => 1,
                'on_install' => 1
            ],
        ]);
    }
}
