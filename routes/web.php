<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.app');
})->middleware(['auth.shop','billable'])->name('home');

Route::get('flush', function(){
    request()->session()->flush();
});

Route::group(['middleware' => ['auth.shop','billable']], function () {
//script
    Route::get('get-scripts', 'Script\ScriptController@index')->name('getscripts');
    Route::post('save-script', 'Script\ScriptController@saveScript')->name('savescript');
    Route::get('delete-script', 'Script\ScriptController@deleteScript')->name('deletescript');
    Route::get('disable-script', 'Script\ScriptController@disableScript')->name('disablescript');

    Route::get('get-script', 'Script\ScriptController@getScript')->name('getscript');

//get-resource
    Route::get('get-resource', 'Script\ScriptController@getResource')->name('getresource');
});
